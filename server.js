const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const path = require('path');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const users = require('./src/routes/api/users');
const profile = require('./src/routes/api/profile');
const posts = require('./src/routes/api/posts');
const checklist = require('./src/routes/api/checklist');
const pays = require('./src/routes/api/pays');
const sejour = require('./src/routes/api/sejour');
const categorie = require('./src/routes/api/categorie');
const vaccin = require('./src/routes/api/vaccin');
const medecin = require('./src/routes/api/medecin');
const allergie = require('./src/routes/api/allergie');
const documents = require('./src/routes/api/documents');
const secours = require('./src/routes/api/secours');
const sanguin = require('./src/routes/api/sanguin');
const maladie = require('./src/routes/api/maladie');
const sante = require('./src/routes/api/sante');
const voyage = require('./src/routes/api/voyage');
const centre = require('./src/routes/api/centre');
const urgence = require('./src/routes/api/urgence');
const User = require('./src/models/User');

// const JwtStrategy = require('passport-jwt').Strategy;
// const ExtractJwt = require('passport-jwt').ExtractJwt;

// const opts = {};
// opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
// opts.secretOrKey = 'jwt';

//  OpenShift sample Node application
const express = require('express');

const corsPrefetch = require('cors-prefetch-middleware').default;
const imagesUpload = require('images-upload-middleware').default;

const app = express();
const morgan = require('morgan');

Object.assign = require('object-assign');

const cors = require('cors');

app.use(cors());
app.options('*', cors());

app.use((req, res, next) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);
  // Pass to next layer of middleware
  next();
});

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.engine('html', require('ejs').renderFile);

app.use(morgan('combined'));

let port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
  // ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || 'http://nodejs-mongo-persistent-vuejs.7e14.starter-us-west-2.openshiftapps.com',
  ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0',
  mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL,
  mongoURLLabel = '';

// eslint-disable-next-line max-len
const db = 'mongodb://reactpwa:reactpwa@reactpwacluster-shard-00-00-2tzai.mongodb.net:27017,reactpwacluster-shard-00-01-2tzai.mongodb.net:27017,reactpwacluster-shard-00-02-2tzai.mongodb.net:27017/test?ssl=true&replicaSet=reactpwaCluster-shard-0&authSource=admin&retryWrites=true';

mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log('Best Best =======>'))
  .catch((e) => {
    console.error('error ==========>', e.message);
  });

mongoose.set('useCreateIndex', true);
// app.use(passport.initialize());
// app.use(passport.session());

// Passport Config
// require('./src/config/passport')(passport);
app.use('/uploads', express.static('./uploads'));

app.use(corsPrefetch);

app.post('/upload', imagesUpload(
  './uploads',
  `${ip}:${port}/uploads`,
  true,
));

// Use Routes
app.use('/api/users', users);
app.use('/api/profile', profile);
app.use('/api/posts', posts);
app.use('/api/checklist', checklist);
app.use('/api/sejour', sejour);
app.use('/api/vaccin', vaccin);
app.use('/api/categorie', categorie);
app.use('/api/medecin', medecin);
app.use('/api/allergie', allergie);
app.use('/api/documents', documents);
app.use('/api/secours', secours);
app.use('/api/sanguin', sanguin);
app.use('/api/pays', pays);
app.use('/api/maladie', maladie);
app.use('/api/sante', sante);
app.use('/api/voyage', voyage);
app.use('/api/centres', centre);
app.use('/api/urgence', urgence);

app.use(express.static('front/build'));

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'front', 'build', 'index.html'));
});

// app.use(express.static('client/build'));

// app.get('*', (req, res) => {
//   res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
// });

app.get('/pagecount', (req, res) => {
  res.json({ msg: 'TES Ok !!' });
});

app.listen(port, ip);
console.log('Server running on http://%s:%s', ip, port);

module.exports = app;
