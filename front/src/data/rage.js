const rage =[
    {
      "id": 1,
      "pays": "Afghanistan",
      "risque": "HAUT"
    },
    {
      "id": 2,
      "pays": "Afrique du Sud",
      "risque": "MODERE"
    },
    {
      "id": 3,
      "pays": "Albanie",
      "risque": "MODERE"
    },
    {
      "id": 4,
      "pays": "Alg‚rie",
      "risque": "HAUT"
    },
    {
      "id": 5,
      "pays": "Allemagne",
      "risque": "FAIBLE"
    },
    {
      "id": 6,
      "pays": "Andorre",
      "risque": "FAIBLE"
    },
    {
      "id": 7,
      "pays": "Angola",
      "risque": "HAUT"
    },
    {
      "id": 8,
      "pays": "×le Christmas",
      "risque": "NON CONNU"
    },
    {
      "id": 9,
      "pays": "×le Norfolk",
      "risque": "NON CONNU"
    },
    {
      "id": 10,
      "pays": "Arabie saoudite",
      "risque": "FAIBLE"
    },
    {
      "id": 11,
      "pays": "Argentine",
      "risque": "FAIBLE"
    },
    {
      "id": 12,
      "pays": "Arm‚nie",
      "risque": "HAUT"
    },
    {
      "id": 13,
      "pays": "×les Cook",
      "risque": "AUCUN"
    },
    {
      "id": 14,
      "pays": "Australie",
      "risque": "FAIBLE"
    },
    {
      "id": 15,
      "pays": "Autriche",
      "risque": "FAIBLE"
    },
    {
      "id": 16,
      "pays": "Azerba‹djan",
      "risque": "HAUT"
    },
    {
      "id": 17,
      "pays": "Bahamas",
      "risque": "MODERE"
    },
    {
      "id": 18,
      "pays": "Bahre‹n",
      "risque": "HAUT"
    },
    {
      "id": 19,
      "pays": "Bangladesh",
      "risque": "HAUT"
    },
    {
      "id": 20,
      "pays": "×les des Cocos (Keeling)",
      "risque": "NON CONNU"
    },
    {
      "id": 21,
      "pays": "Belgique",
      "risque": "FAIBLE"
    },
    {
      "id": 22,
      "pays": "Belize",
      "risque": "MODERE"
    },
    {
      "id": 23,
      "pays": "B‚nin",
      "risque": "HAUT"
    },
    {
      "id": 24,
      "pays": "Bermudes",
      "risque": "FAIBLE"
    },
    {
      "id": 25,
      "pays": "Bhoutan",
      "risque": "HAUT"
    },
    {
      "id": 26,
      "pays": "Bi‚lorussie",
      "risque": "MODERE"
    },
    {
      "id": 27,
      "pays": "Bolivie (tat plurinational de)",
      "risque": "HAUT"
    },
    {
      "id": 28,
      "pays": "×les Wallis-et-Futuna",
      "risque": "AUCUN"
    },
    {
      "id": 29,
      "pays": "Bosnie-Herz‚govine",
      "risque": "MODERE"
    },
    {
      "id": 30,
      "pays": "Botswana",
      "risque": "MODERE"
    },
    {
      "id": 31,
      "pays": "Br‚sil",
      "risque": "MODERE"
    },
    {
      "id": 32,
      "pays": "Brun‚i Darussalam",
      "risque": "HAUT"
    },
    {
      "id": 33,
      "pays": "Bulgarie",
      "risque": "MODERE"
    },
    {
      "id": 34,
      "pays": "Burkina Faso",
      "risque": "HAUT"
    },
    {
      "id": 35,
      "pays": "Burundi",
      "risque": "HAUT"
    },
    {
      "id": 36,
      "pays": "Cambodge",
      "risque": "HAUT"
    },
    {
      "id": 37,
      "pays": "Cameroun",
      "risque": "HAUT"
    },
    {
      "id": 38,
      "pays": "Canada",
      "risque": "FAIBLE"
    },
    {
      "id": 39,
      "pays": "Cap-Vert",
      "risque": "MODERE"
    },
    {
      "id": 40,
      "pays": "Chili",
      "risque": "FAIBLE"
    },
    {
      "id": 41,
      "pays": "Chine",
      "risque": "HAUT"
    },
    {
      "id": 42,
      "pays": "Chypre",
      "risque": "FAIBLE"
    },
    {
      "id": 43,
      "pays": "Colombie",
      "risque": "MODERE"
    },
    {
      "id": 44,
      "pays": "Nauru",
      "risque": "AUCUN"
    },
    {
      "id": 45,
      "pays": "Congo",
      "risque": "HAUT"
    },
    {
      "id": 46,
      "pays": "Congo (R‚publique d‚mocratique du)",
      "risque": "HAUT"
    },
    {
      "id": 47,
      "pays": "Cor‚e du Nord (r‚p. pop. d‚m. de)",
      "risque": "HAUT"
    },
    {
      "id": 48,
      "pays": "Cor‚e du Sud (r‚p. de)",
      "risque": "HAUT"
    },
    {
      "id": 49,
      "pays": "Costa Rica",
      "risque": "MODERE"
    },
    {
      "id": 50,
      "pays": "C“te d?Ivoire",
      "risque": "HAUT"
    },
    {
      "id": 51,
      "pays": "Croatie",
      "risque": "MODERE"
    },
    {
      "id": 52,
      "pays": "Cuba",
      "risque": "HAUT"
    },
    {
      "id": 53,
      "pays": "Niou‚",
      "risque": "AUCUN"
    },
    {
      "id": 54,
      "pays": "Danemark",
      "risque": "FAIBLE"
    },
    {
      "id": 55,
      "pays": "Djibouti",
      "risque": "HAUT"
    },
    {
      "id": 56,
      "pays": "Pitcairn",
      "risque": "AUCUN"
    },
    {
      "id": 57,
      "pays": "gypte",
      "risque": "HAUT"
    },
    {
      "id": 58,
      "pays": "El Salvador",
      "risque": "HAUT"
    },
    {
      "id": 59,
      "pays": "mirats arabes unis",
      "risque": "FAIBLE"
    },
    {
      "id": 60,
      "pays": "quateur",
      "risque": "MODERE"
    },
    {
      "id": 61,
      "pays": "rythr‚e",
      "risque": "HAUT"
    },
    {
      "id": 62,
      "pays": "Espagne",
      "risque": "FAIBLE"
    },
    {
      "id": 63,
      "pays": "Estonie",
      "risque": "FAIBLE"
    },
    {
      "id": 64,
      "pays": "Eswatini",
      "risque": "MODERE"
    },
    {
      "id": 65,
      "pays": "tats-Unis d?Am‚rique",
      "risque": "FAIBLE"
    },
    {
      "id": 66,
      "pays": "thiopie",
      "risque": "HAUT"
    },
    {
      "id": 67,
      "pays": "Fidji",
      "risque": "AUCUN"
    },
    {
      "id": 68,
      "pays": "Finlande",
      "risque": "FAIBLE"
    },
    {
      "id": 69,
      "pays": "France",
      "risque": "FAIBLE"
    },
    {
      "id": 70,
      "pays": "Gabon",
      "risque": "HAUT"
    },
    {
      "id": 71,
      "pays": "Gambie",
      "risque": "HAUT"
    },
    {
      "id": 72,
      "pays": "G‚orgie",
      "risque": "HAUT"
    },
    {
      "id": 73,
      "pays": "Polyn‚sie fran‡aise",
      "risque": "AUCUN"
    },
    {
      "id": 74,
      "pays": "Ghana",
      "risque": "HAUT"
    },
    {
      "id": 75,
      "pays": "Gibraltar",
      "risque": "NON CONNU"
    },
    {
      "id": 76,
      "pays": "GrŠce",
      "risque": "FAIBLE"
    },
    {
      "id": 77,
      "pays": "Samoa",
      "risque": "AUCUN"
    },
    {
      "id": 78,
      "pays": "Groenland",
      "risque": "FAIBLE"
    },
    {
      "id": 79,
      "pays": "Samoa am‚ricaines",
      "risque": "AUCUN"
    },
    {
      "id": 80,
      "pays": "Guam",
      "risque": "NON CONNU"
    },
    {
      "id": 81,
      "pays": "Guatemala",
      "risque": "HAUT"
    },
    {
      "id": 82,
      "pays": "Guernesey",
      "risque": "FAIBLE"
    },
    {
      "id": 83,
      "pays": "Guin‚e",
      "risque": "HAUT"
    },
    {
      "id": 84,
      "pays": "Guin‚e ‚quatoriale",
      "risque": "HAUT"
    },
    {
      "id": 85,
      "pays": "Guin‚e-Bissau",
      "risque": "HAUT"
    },
    {
      "id": 86,
      "pays": "Guyana",
      "risque": "FAIBLE"
    },
    {
      "id": 87,
      "pays": "Guyane fran‡aise",
      "risque": "FAIBLE"
    },
    {
      "id": 88,
      "pays": "Ha‹ti",
      "risque": "HAUT"
    },
    {
      "id": 89,
      "pays": "Honduras",
      "risque": "HAUT"
    },
    {
      "id": 90,
      "pays": "Hongrie",
      "risque": "FAIBLE"
    },
    {
      "id": 91,
      "pays": "Tok‚laou",
      "risque": "AUCUN"
    },
    {
      "id": 92,
      "pays": "×le de Man",
      "risque": "FAIBLE"
    },
    {
      "id": 93,
      "pays": "Tonga",
      "risque": "AUCUN"
    },
    {
      "id": 94,
      "pays": "×les Ca‹manes",
      "risque": "FAIBLE"
    },
    {
      "id": 95,
      "pays": "Anguilla",
      "risque": "FAIBLE"
    },
    {
      "id": 96,
      "pays": "Antigua-et-Barbuda",
      "risque": "FAIBLE"
    },
    {
      "id": 97,
      "pays": "Aruba",
      "risque": "FAIBLE"
    },
    {
      "id": 98,
      "pays": "Barbade",
      "risque": "FAIBLE"
    },
    {
      "id": 99,
      "pays": "Bonaire",
      "risque": "FAIBLE"
    },
    {
      "id": 100,
      "pays": "×les Mariannes du Nord",
      "risque": "NON CONNU"
    },
    {
      "id": 101,
      "pays": "×les Marshall",
      "risque": "AUCUN"
    },
    {
      "id": 102,
      "pays": "×les Salomon",
      "risque": "AUCUN"
    },
    {
      "id": 103,
      "pays": "Cura‡ao",
      "risque": "FAIBLE"
    },
    {
      "id": 104,
      "pays": "Dominique",
      "risque": "FAIBLE"
    },
    {
      "id": 105,
      "pays": "G‚orgie du Sud-et-les ×les Sandwich du Sud",
      "risque": "FAIBLE"
    },
    {
      "id": 106,
      "pays": "Guadeloupe",
      "risque": "FAIBLE"
    },
    {
      "id": 107,
      "pays": "×les d?land",
      "risque": "FAIBLE"
    },
    {
      "id": 108,
      "pays": "Inde",
      "risque": "HAUT"
    },
    {
      "id": 109,
      "pays": "Indon‚sie",
      "risque": "HAUT"
    },
    {
      "id": 110,
      "pays": "Irak",
      "risque": "HAUT"
    },
    {
      "id": 111,
      "pays": "Iran (R‚publique islamique d?)",
      "risque": "HAUT"
    },
    {
      "id": 112,
      "pays": "Irlande",
      "risque": "FAIBLE"
    },
    {
      "id": 113,
      "pays": "Islande",
      "risque": "FAIBLE"
    },
    {
      "id": 114,
      "pays": "Isra‰l",
      "risque": "HAUT"
    },
    {
      "id": 115,
      "pays": "Italie",
      "risque": "FAIBLE"
    },
    {
      "id": 116,
      "pays": "Jama‹que",
      "risque": "FAIBLE"
    },
    {
      "id": 117,
      "pays": "Japon",
      "risque": "AUCUN"
    },
    {
      "id": 118,
      "pays": "Jersey",
      "risque": "FAIBLE"
    },
    {
      "id": 119,
      "pays": "Jordanie",
      "risque": "HAUT"
    },
    {
      "id": 120,
      "pays": "Kazakhstan",
      "risque": "HAUT"
    },
    {
      "id": 121,
      "pays": "Kenya",
      "risque": "HAUT"
    },
    {
      "id": 122,
      "pays": "Kirghizistan",
      "risque": "HAUT"
    },
    {
      "id": 123,
      "pays": "Kiribati",
      "risque": "AUCUN"
    },
    {
      "id": 124,
      "pays": "Kowe‹t",
      "risque": "FAIBLE"
    },
    {
      "id": 125,
      "pays": "Laos (R‚publique d‚mocratique populaire)",
      "risque": "HAUT"
    },
    {
      "id": 126,
      "pays": "Lesotho",
      "risque": "MODERE"
    },
    {
      "id": 127,
      "pays": "Lettonie",
      "risque": "FAIBLE"
    },
    {
      "id": 128,
      "pays": "Liban",
      "risque": "HAUT"
    },
    {
      "id": 129,
      "pays": "Lib‚ria",
      "risque": "HAUT"
    },
    {
      "id": 130,
      "pays": "Libye",
      "risque": "HAUT"
    },
    {
      "id": 131,
      "pays": "Liechtenstein",
      "risque": "FAIBLE"
    },
    {
      "id": 132,
      "pays": "Lituanie",
      "risque": "FAIBLE"
    },
    {
      "id": 133,
      "pays": "Luxembourg",
      "risque": "FAIBLE"
    },
    {
      "id": 134,
      "pays": "Mac‚doine",
      "risque": "FAIBLE"
    },
    {
      "id": 135,
      "pays": "Madagascar",
      "risque": "MODERE"
    },
    {
      "id": 136,
      "pays": "Malaisie",
      "risque": "MODERE"
    },
    {
      "id": 137,
      "pays": "Malawi",
      "risque": "HAUT"
    },
    {
      "id": 138,
      "pays": "Maldives",
      "risque": "FAIBLE"
    },
    {
      "id": 139,
      "pays": "Mali",
      "risque": "HAUT"
    },
    {
      "id": 140,
      "pays": "×les Falkland (Malvinas)",
      "risque": "FAIBLE"
    },
    {
      "id": 141,
      "pays": "Maroc",
      "risque": "HAUT"
    },
    {
      "id": 142,
      "pays": "×les F‚ro‚",
      "risque": "FAIBLE"
    },
    {
      "id": 143,
      "pays": "Maurice",
      "risque": "FAIBLE"
    },
    {
      "id": 144,
      "pays": "Mauritanie",
      "risque": "HAUT"
    },
    {
      "id": 145,
      "pays": "Mayotte",
      "risque": "FAIBLE"
    },
    {
      "id": 146,
      "pays": "Mexique",
      "risque": "MODERE"
    },
    {
      "id": 147,
      "pays": "Micron‚sie (tats f‚d‚r‚s de)",
      "risque": "AUCUN"
    },
    {
      "id": 148,
      "pays": "Moldavie",
      "risque": "MODERE"
    },
    {
      "id": 149,
      "pays": "Monaco",
      "risque": "FAIBLE"
    },
    {
      "id": 150,
      "pays": "Mongolie",
      "risque": "HAUT"
    },
    {
      "id": 151,
      "pays": "Mont‚n‚gro",
      "risque": "MODERE"
    },
    {
      "id": 152,
      "pays": "×les Svalbard-et-Jan Mayen",
      "risque": "FAIBLE"
    },
    {
      "id": 153,
      "pays": "Mozambique",
      "risque": "HAUT"
    },
    {
      "id": 154,
      "pays": "Myanmar",
      "risque": "HAUT"
    },
    {
      "id": 155,
      "pays": "Namibie",
      "risque": "MODERE"
    },
    {
      "id": 156,
      "pays": "×les Turques-et-Ca‹ques",
      "risque": "FAIBLE"
    },
    {
      "id": 157,
      "pays": "N‚pal",
      "risque": "HAUT"
    },
    {
      "id": 158,
      "pays": "Nicaragua",
      "risque": "MODERE"
    },
    {
      "id": 159,
      "pays": "Niger",
      "risque": "HAUT"
    },
    {
      "id": 160,
      "pays": "Nig‚ria",
      "risque": "HAUT"
    },
    {
      "id": 161,
      "pays": "×les Vierges am‚ricaines",
      "risque": "FAIBLE"
    },
    {
      "id": 162,
      "pays": "NorvŠge",
      "risque": "FAIBLE"
    },
    {
      "id": 163,
      "pays": "×les Vierges britanniques",
      "risque": "FAIBLE"
    },
    {
      "id": 164,
      "pays": "Nouvelle-Z‚lande",
      "risque": "AUCUN"
    },
    {
      "id": 165,
      "pays": "Oman",
      "risque": "FAIBLE"
    },
    {
      "id": 166,
      "pays": "Ouganda",
      "risque": "HAUT"
    },
    {
      "id": 167,
      "pays": "Ouzb‚kistan",
      "risque": "HAUT"
    },
    {
      "id": 168,
      "pays": "Pakistan",
      "risque": "HAUT"
    },
    {
      "id": 169,
      "pays": "Palaos",
      "risque": "AUCUN"
    },
    {
      "id": 170,
      "pays": "Palestine",
      "risque": "HAUT"
    },
    {
      "id": 171,
      "pays": "Panama",
      "risque": "MODERE"
    },
    {
      "id": 172,
      "pays": "Papouasie-Nouvelle-Guin‚e",
      "risque": "FAIBLE"
    },
    {
      "id": 173,
      "pays": "Paraguay",
      "risque": "MODERE"
    },
    {
      "id": 174,
      "pays": "Pays-Bas",
      "risque": "FAIBLE"
    },
    {
      "id": 175,
      "pays": "P‚rou",
      "risque": "MODERE"
    },
    {
      "id": 176,
      "pays": "Philippines",
      "risque": "HAUT"
    },
    {
      "id": 177,
      "pays": "Malte",
      "risque": "FAIBLE"
    },
    {
      "id": 178,
      "pays": "Pologne",
      "risque": "FAIBLE"
    },
    {
      "id": 179,
      "pays": "Martinique",
      "risque": "FAIBLE"
    },
    {
      "id": 180,
      "pays": "Porto Rico",
      "risque": "FAIBLE"
    },
    {
      "id": 181,
      "pays": "Portugal",
      "risque": "FAIBLE"
    },
    {
      "id": 182,
      "pays": "Qatar",
      "risque": "FAIBLE"
    },
    {
      "id": 183,
      "pays": "R‚publique centrafricaine",
      "risque": "HAUT"
    },
    {
      "id": 184,
      "pays": "R‚publique dominicaine",
      "risque": "HAUT"
    },
    {
      "id": 185,
      "pays": "R‚publique tchŠque",
      "risque": "FAIBLE"
    },
    {
      "id": 186,
      "pays": "R‚union",
      "risque": "MODERE"
    },
    {
      "id": 187,
      "pays": "Roumanie",
      "risque": "MODERE"
    },
    {
      "id": 188,
      "pays": "Royaume-Uni",
      "risque": "FAIBLE"
    },
    {
      "id": 189,
      "pays": "Russie (F‚d‚ration de)",
      "risque": "MODERE"
    },
    {
      "id": 190,
      "pays": "Rwanda",
      "risque": "HAUT"
    },
    {
      "id": 191,
      "pays": "Montserrat",
      "risque": "FAIBLE"
    },
    {
      "id": 192,
      "pays": "Sahara occidental",
      "risque": "HAUT"
    },
    {
      "id": 193,
      "pays": "Nouvelle-Cal‚donie",
      "risque": "FAIBLE"
    },
    {
      "id": 194,
      "pays": "Saba",
      "risque": "FAIBLE"
    },
    {
      "id": 195,
      "pays": "Saint-Barth‚lemy",
      "risque": "FAIBLE"
    },
    {
      "id": 196,
      "pays": "Sainte-H‚lŠne",
      "risque": "FAIBLE"
    },
    {
      "id": 197,
      "pays": "Sainte-Lucie",
      "risque": "FAIBLE"
    },
    {
      "id": 198,
      "pays": "Saint-Marin",
      "risque": "FAIBLE"
    },
    {
      "id": 199,
      "pays": "Saint-Eustache",
      "risque": "FAIBLE"
    },
    {
      "id": 200,
      "pays": "Saint-Kitts-et-Nevis",
      "risque": "FAIBLE"
    },
    {
      "id": 201,
      "pays": "Saint-Pierre-et-Miquelon",
      "risque": "FAIBLE"
    },
    {
      "id": 202,
      "pays": "Saint-Martin (partie fran‡aise)",
      "risque": "FAIBLE"
    },
    {
      "id": 203,
      "pays": "Saint-Martin (partie n‚erlandaise)",
      "risque": "FAIBLE"
    },
    {
      "id": 204,
      "pays": "Saint-Vincent-et-les Grenadines",
      "risque": "FAIBLE"
    },
    {
      "id": 205,
      "pays": "Sao Tom‚-et-Principe",
      "risque": "MODERE"
    },
    {
      "id": 206,
      "pays": "S‚n‚gal",
      "risque": "HAUT"
    },
    {
      "id": 207,
      "pays": "Serbie",
      "risque": "MODERE"
    },
    {
      "id": 208,
      "pays": "Sercq",
      "risque": "FAIBLE"
    },
    {
      "id": 209,
      "pays": "Seychelles",
      "risque": "FAIBLE"
    },
    {
      "id": 210,
      "pays": "Sierra Leone",
      "risque": "HAUT"
    },
    {
      "id": 211,
      "pays": "Singapour",
      "risque": "FAIBLE"
    },
    {
      "id": 212,
      "pays": "Slovaquie",
      "risque": "FAIBLE"
    },
    {
      "id": 213,
      "pays": "Slov‚nie",
      "risque": "FAIBLE"
    },
    {
      "id": 214,
      "pays": "Somalie",
      "risque": "HAUT"
    },
    {
      "id": 215,
      "pays": "Soudan",
      "risque": "HAUT"
    },
    {
      "id": 216,
      "pays": "Soudan du Sud",
      "risque": "HAUT"
    },
    {
      "id": 217,
      "pays": "Sri Lanka",
      "risque": "HAUT"
    },
    {
      "id": 218,
      "pays": "SuŠde",
      "risque": "FAIBLE"
    },
    {
      "id": 219,
      "pays": "Suisse",
      "risque": "FAIBLE"
    },
    {
      "id": 220,
      "pays": "Suriname",
      "risque": "FAIBLE"
    },
    {
      "id": 221,
      "pays": "Syrie",
      "risque": "HAUT"
    },
    {
      "id": 222,
      "pays": "Tadjikistan",
      "risque": "HAUT"
    },
    {
      "id": 223,
      "pays": "Ta‹wan",
      "risque": "HAUT"
    },
    {
      "id": 224,
      "pays": "Tanzanie (R‚publique-Unie de)",
      "risque": "HAUT"
    },
    {
      "id": 225,
      "pays": "Tchad",
      "risque": "HAUT"
    },
    {
      "id": 226,
      "pays": "Comores",
      "risque": "MODERE"
    },
    {
      "id": 227,
      "pays": "Grenade",
      "risque": "MODERE"
    },
    {
      "id": 228,
      "pays": "Tha‹lande",
      "risque": "MODERE"
    },
    {
      "id": 229,
      "pays": "Timor-Leste",
      "risque": "HAUT"
    },
    {
      "id": 230,
      "pays": "Togo",
      "risque": "HAUT"
    },
    {
      "id": 231,
      "pays": "Terres australes fran‡aises",
      "risque": "NON CONNU"
    },
    {
      "id": 232,
      "pays": "Territoire britannique de l'oc‚an Indien",
      "risque": "NON CONNU"
    },
    {
      "id": 233,
      "pays": "Trinit‚-et-Tobago",
      "risque": "FAIBLE"
    },
    {
      "id": 234,
      "pays": "Tunisie",
      "risque": "HAUT"
    },
    {
      "id": 235,
      "pays": "Turkm‚nistan",
      "risque": "HAUT"
    },
    {
      "id": 236,
      "pays": "Turquie",
      "risque": "HAUT"
    },
    {
      "id": 237,
      "pays": "Tuvalu",
      "risque": "AUCUN"
    },
    {
      "id": 238,
      "pays": "Ukraine",
      "risque": "MODERE"
    },
    {
      "id": 239,
      "pays": "Uruguay",
      "risque": "FAIBLE"
    },
    {
      "id": 240,
      "pays": "Vanuatu",
      "risque": "AUCUN"
    },
    {
      "id": 241,
      "pays": "Vatican",
      "risque": "FAIBLE"
    },
    {
      "id": 242,
      "pays": "Venezuela (R‚publique bolivarienne du)",
      "risque": "MODERE"
    },
    {
      "id": 243,
      "pays": "Viet Nam",
      "risque": "HAUT"
    },
    {
      "id": 244,
      "pays": "Y‚men",
      "risque": "HAUT"
    },
    {
      "id": 245,
      "pays": "Zambie",
      "risque": "MODERE"
    },
    {
      "id": 246,
      "pays": "Zimbabwe",
      "risque": "MODERE"
    }
  ]

export default rage;
