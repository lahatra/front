import { loginUser, registerUser, logoutUser, updateUsers, getData } from './actions/authActions';
import { getVoyage, addVoyage } from './actions/VoyageActions';
import { getPays } from './actions/PaysActions';
import { getSejour, getSejours } from './actions/SejourActions';
import { getMaladie } from './actions/MaladieActions';
import { getQuestions, addLikeChecklist, getChecklistUser } from './actions/QuestionActions';
import { getSecours, addLikeSecours, getSecoursUser } from './actions/SecoursActions';
import { getSante, getSanteUser, addSante, updateSante } from './actions/SanteActions';
import { getSanguin } from './actions/SanguinActions';
import { getAllergie } from './actions/AllergieActions';
import { getVaccin } from './actions/VaccinActions';

const mapDispatchToProps = {
  loginUser,
  getPays,
  getVoyage,
  registerUser,
  addVoyage,
  getSejour,
  getSejours,
  getMaladie,
  getQuestions,
  addLikeChecklist,
  getChecklistUser,
  logoutUser,
  getSecours,
  addLikeSecours,
  getSecoursUser,
  getSante,
  getSanguin,
  getAllergie,
  getSanteUser,
  addSante,
  updateSante,
  updateUsers,
  getVaccin,
  getData,
};

export default mapDispatchToProps;
