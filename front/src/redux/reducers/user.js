import {
  GET_VOYAGES,
  LOGIN,
} from '../actions/user';


const userdata = {
  voyages: [],
  user: {},
};

export default (state = userdata, action) => {
  switch (action.type) {
    case GET_VOYAGES:
      return {
        ...state,
        voyages: action.payload,
      };
    case LOGIN:
      return {
        ...state,
        user: action.payload,
      };
  }
  return state;
};
