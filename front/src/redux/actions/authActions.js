import axios from 'axios';
import jwt_decode from 'jwt-decode';

import setAuthToken from '../utils/setAuthToken';
import { GET_ERRORS, SET_CURRENT_USER, UPDATE_USERS } from './types';
import config from '../utils/config';

export const updateUsers = data => (dispatch) => {
  axios
    .put(`${config.baseUrl}/api/users/${data.id}`, data)
    .then(res =>
      dispatch({
        type: UPDATE_USERS,
        payload: res.data,
      }),
    )
    .catch(() => {});
};

// Set logged in user
export const setCurrentUser = decoded => ({
  type: SET_CURRENT_USER,
  payload: decoded,
});

// Login - Get User Token
export const loginUser = (userData, cb) => (dispatch) => {
  axios
    .post(`${config.baseUrl}/api/users/login`, userData)
    .then((res) => {
      // Save to localStorage
      const { token } = res.data;
      // Set token to ls
      localStorage.setItem('jwtToken', token);
      // Set token to Auth header
      setAuthToken(token);
      // Decode token to get user data
      const decoded = jwt_decode(token);
      // Set current user
      dispatch(setCurrentUser({ ...decoded, token }));
      cb && cb();
    })
    .catch((err) => {
      const payload = err.response ? err.response.data : err;
      dispatch({
        type: GET_ERRORS,
        payload,
      });
    });
};

// Register User
export const registerUser = (userData, cb) => (dispatch) => {
  axios
    .post(`${config.baseUrl}/api/users/register`, userData)
    .then((res) => {
      dispatch(loginUser({ email: userData.email, password: userData.password }, cb));
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};

// Register User
export const getData = () => (dispatch, getState) => {
  const user = getState().auth.user ? getState().auth.user.id : null;
  if (!user) return;
  axios
    .get(`${config.baseUrl}/api/users/data${user}`)
    .then((res) => {
      console.log('res', res);
      dispatch();
    })
    .catch(() => {});
};

// Log user out
export const logoutUser = () => (dispatch) => {
  // Remove token from localStorage
  localStorage.removeItem('jwtToken');
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
};
