import React, { Component } from 'react';
import Header4 from '../components/header4';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

class CarnetVaccination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vaccinAll: [],
      paysCurrent: {},
      maladieCurrent: [],
      vaccin: '',
      date: '',
      i: 0,
    };
  }

  componentWillMount() {
    const { one_voyage } = this.props.voyage;
    const { pays } = this.props.pays;
    const { maladie } = this.props.maladie;
    const { vaccin } = this.props.vaccin;
    vaccin && this.setState({ vaccinAll: vaccin });
    if (!pays) return;
    const paysID = pays.filter(i => one_voyage.pays === i._id);
    paysID && this.setState({ paysCurrent: paysID[0] });
    const maladieIDS = paysID[0].maladie && paysID[0].maladie.map(i => i._id);
    const maladieCurrent = maladie.filter(i => maladieIDS.includes(i._id));
    maladieCurrent && this.setState({ maladieCurrent });
    // if (paysID && paysID.length > 0) {
    //   const maladieIDS = paysID[0].maladie && paysID[0].maladie.map(i => i._id);
    //   const res = {
    //     v1: [],
    //     v2: [],
    //   };

    //   const vaccinDetail = maladie && maladie.reduce((acc, curr) => {
    //     if (maladieIDS && maladieIDS.includes(curr._id)) {
    //       const mIDS = curr.vaccin.map(i => i._id);
    //       const s = vaccin && vaccin.filter(i => mIDS.includes(i._id));
    //       s && acc.v1.push(...s);
    //       const mIDSugg = curr.vaccinSugg.map(i => i._id);
    //       const s2 = vaccin && vaccin.filter(i => mIDSugg.includes(i._id));
    //       s2 && acc.v2.push(...s2);
    //     }
    //     return acc;
    //   }, res);
    //   const vs = vaccinDetail ? { v1: getUnique(vaccinDetail.v1, '_id'), v2: getUnique(vaccinDetail.v2, '_id') } : [];
    //   console.log('vaccin', vs);
    //   this.setState({ vaccin: vs });
    // }
    // this.props.getSejour(maladie[0]._id);
  }

  addVaccin = () => {
    const { vaccin, id, name, email, avatar } = this.props.auth.user;
    const newUser = {
      id,
      name,
      email,
      avatar,
	    vaccin: [...vaccin, { _id: this.state.vaccin, date: this.state.date }],
	  };
    this.props.updateUsers(newUser);
  }

  render() {
    const { vaccinAll, vaccin, date } = this.state;
    return (
      <div>
        <Header4 />
        <div className="vaccin-page">
          <div id="titre">
            <label className="title">Carnet de vaccination</label>
          </div>
          <div id="vaccin-form">
            <div className="element">
              <select
                name="vaccin"
                size="1"
                onChange={e => this.setState({ vaccin: e.target.value })}
                value={vaccin}
              >
                {vaccinAll.map(i => <option key={i._id} value={i._id}>{i.name}</option>)}
                {/* <option value="#">VACCIN0</option>
                <option value="#">VACCIN1</option>
                <option value="#">VACCIN2</option>
                <option value="#">VACCIN3</option> */}
              </select>
            </div>
            <div className="element">
              <select
                name="date"
                size="1"
                value={date}
                onChange={e => this.setState({ date: e.target.value })}
              >
                <option value="2017">2017</option>
                <option value="2018">2018</option>
                <option value="2019">2019</option>
                <option value="2020">2020</option>
              </select>
            </div>
            <div className="element">
              <div className="add-button">
                <button value="+" onClick={this.addVaccin}> + </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CarnetVaccination);
