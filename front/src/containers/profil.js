import React, { Component } from 'react';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nom: '',
      email: '',
      password: '',
      pays: '',
      date: '',
      password1: '',
    };
  }

  register = () => {
    this.props.register(this.state);
    console.log('registerregister', this.state);
    this.props.history.push('/premiervoyage');
  }

  render() {
    return (
      <div>
        <div ClassName="login-page">
          <div id="titre">
            <div ClassName="element">
              <label ClassName="title">Créer Un Compte </label>
            </div>
          </div>

          <div id="conteneur">
            <div ClassName="element">
              <img src="../img/policeman.png" ClassName="icone" alt="urgence-police-travelkit" />
            </div>
            <div ClassName="element">
              <img src="../img/cardio.png" ClassName="icone" alt="urgence-police-travelkit" />
            </div>
          </div>

          <div ClassName="form">
            <form ClassName="login-form">
              <select name="pays" size="1">
                <option>France</option>
                <option>Suisse</option>
                <option>Madagascar</option>
                <option>La Réunion</option>
              </select>
              <input
                value={this.state.nom}
                onChange={e => this.setState({ nom: e.target.value })}
                type="text"
                placeholder="Nom et Prénom"
              />
              <input
                value={this.state.email}
                onChange={e => this.setState({ email: e.target.value })}
                type="mail"
                placeholder="E-mail"
              />
              <input
                value={this.state.date}
                onChange={e => this.setState({ date: e.target.value })}
                type="date"
                placeholder="Date de Naissance"
              />
              <input
                value={this.state.password}
                onChange={e => this.setState({ password: e.target.value })}
                type="password"
                placeholder="Mot de passe"
              />
              <input
                value={this.state.password1}
                onChange={e => this.setState({ password1: e.target.value })}
                type="password"
                placeholder="Confirmation mot de passe"
              />
              <button onClick={this.register}>Connexion</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);
