import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class Info extends Component {

	render() {
	  return (
		  <div>
			  <Header4/>
  <div className="sante-page">				
		<div id="titre">
			<div className ="element">
			<label className="title">Tiques</label>
			</div>
		</div>
	
		<div className="group">
		<label className="description">
		D’après mon profil voyageur, les piqûres de tiques présentent un risque durant mon voyage. Des mesures de préventions simples permettent de profiter du voyage sans s’en soucier !
		<br></br><br></br><br></br>
En résumé : 
<br></br><br></br>
- Avant le voyage : 
<br></br><br></br>
           J’imprègne mes vêtements avec un produit insecticide prévu à cet effet ;
		   <br></br><br></br>
- Sur place : 
<br></br><br></br>
<ul className="shortcut">
			<li>  Je porte des vêtements amples et couvrants lors des balades dans les zones boisées ou rurales .</li>
		  
		  <li>		   J’utilise un répulsif pour la peau adapté .</li>
         
		   <li>   Après les balades, je vérifie systématiquement sur l’ensemble du corps la présence d’éventuelles tiques (y compris les cheveux, derrière les oreilles, entre les orteils, etc…), les retire et désinfecte la peau .</li>

		   </ul>        
		</label>
		</div>
		<div id="conteneur">
		<div className="element">
		<img className="prise"src="../img/protection_tique.png" alt="prise"/>
		</div>
		</div>
		<br></br>

	
		<div className="tab blue">
				<input id="tab-four" type="radio" name="tabs2"/>
					<label for="tab-four">Comment choisir un répulsif 
pour la peau ?</label>
				<div className="tab-content">
					<p>
					Il peut être nécessaire de me protéger contre les piqûres d’insectes pour de multiples raisons. Les protections les plus efficaces sont l’application de répulsifs cutanés sur les parties non couvertes du corps, l’utilisation de vêtements amples et couvrants, voire imprégnés d’insecticides, et dormir la nuit sous une moustiquaire imprégnée (correctement installée et en s’assurant de l’intégrité du maillage). Ils sont disponibles en pharmacie sans prescription ou dans les magasins spécialisés de voyage.

Dans les habitations, la climatisation diminue les risques de piqûres. Elle peut être utilisée en mesure d’appoint, tout comme les insecticides (en spray ou diffuseur) ainsi que les raquettes électriques ; mais sont moins efficaces que les méthodes précédentes.
À l’extérieur, les serpentins fumigènes peuvent également être utilisés.
Il est également recommandé de ne pas laisser stagner de l’eau dans des récipients, car cela favorise la multiplication des moustiques.

					</p>
					<br></br>
					<p>
					Les moustiquaires grillagées aux portes et fenêtres sont presque aussi efficaces que les moustiquaires imprégnées et constitue une bonne protection.
Pour se protéger des piqûres de moustique en journée ou en soirée, l’usage de répulsifs cutanés et de vêtements imprégnés est fortement recommandé. 

En revanche, les produits suivants ne sont pas (suffisamment) efficaces :
Les bracelets anti-insectes pour se protéger des moustiques et des tiques ;
Les huiles essentielles : lorsqu’elles sont efficaces, leur durée d’efficacité est limitée (généralement une vingtaine de minutes).
Autres méthodes non ou insuffisamment efficaces : les appareils sonores à ultrasons, la vitamine B1, l’homéopathie, les rubans, papiers et autocollants gluants sans insecticide.

					</p>
				</div>
		</div>
		<div className="tab blue">
				<input id="tab-five" type="radio" name="tabs2"/>
					<label for="tab-five">Comment utiliser un répulsif 
pour la peau ?</label>
				<div className="tab-content">
					<p>Lorem ipsum dolor </p>
					
				</div>
		</div>

		<div className="tab blue">
				<input id="tab-six" type="radio" name="tabs2"/>
					<label for="tab-six">Comment retirer une tique ?</label>
				<div className="tab-content">
					<p>Après les balades,<strong>je vérifie systématiquement sur l’ensemble du corps</strong> je vérifie systématiquement sur l’ensemble du corps la présence d’éventuelles tiques (y compris les cheveux, derrière les oreilles, entre les orteils, etc…). Une tique ne saute pas, elle s’accroche au passage puis choisit un endroit pour se nourrir, elle peut donc se retrouver n’importe où sur le corps (ou sur les vêtements, d’où l’intérêt de vérifier la présence de tiques plusieurs jours d’affilée). Les petites tiques sont parfois difficiles à voir, cependant on les sent bien lorsque on passe la main sur le corps. 
					<br></br>

<strong>Une tique peut se retirer sans difficulté, même quand on ne l’a jamais fait auparavant.</strong>
<br></br>
Pour cela, un tire-tique ou une pince à épiler peut être utilisé : 
<br></br>
<ul>
<li> Je saisis la tique avec la pince à épiler ou le tire-tique délicatement le plus près possible de la peau.</li>
<li>Je fais tourner la tique sur elle-même.</li>
<li>Je la retire ensuite en tirant sans forcer.</li>
<li>Je désinfecte systématique la plaie, même si elle est minuscule, après le retrait (pas avant) !</li>
</ul>
<div id="conteneur">
		<div className="element">
		<img className="image"src="../img/tique_pince.png" alt="pince"/>
		</div>
		<div className="element">
		<img className="image"src="../img/tire_tique.png" alt="pince"/>
		</div>
		</div>
		<p>
		La tique s’accroche fortement à la peau, il est courant de ne pas réussir du premier coup. L’important est de réessayer sans forcer.
<br></br>
On entend beaucoup de choses à propos du retrait des tiques : application de produit, méthode pour brûler, etc… Ces méthodes sont au mieux inefficaces, au pire dangereuses en particulier l’application de produit (quel qu’il soit) qui risque de faire régurgiter la tique dans la peau, avec tous les microbes qui peuvent l’accompagner…

		</p>


</p>
					
				</div>
		</div>
		<div className="tab blue">
				<input id="tab-seven" type="radio" name="tabs2"/>
					<label for="tab-seven">Dois-je consulter après une
piqûre de tique ?</label>
				<div className="tab-content">
					<p>Lorem ipsum dolor </p>
					
				</div>
		</div>
		
		<div className="group">
					<div className="footer">
				<Link to="/info">
					<img src="../img/info.png" className="image_footer" alt="Infos"/>
				</Link>
				<Link to="/moustique">
					<img src="../img/moustique.png" className="image_footer" alt="Moustique"/>
				</Link>
				<Link to="/soleil">
					<img src="/img/soleil.png" className="image_footer" alt="soleil2"/>
				</Link>
				<Link to="/baignade">
					<img src="../img/baignade.png" className="image_footer" alt="baignade"/>
				</Link>
				<Link to="/animaux">
					<img src="../img/animaux.png" className="image_footer" alt="renard"/>
				</Link>
				<Link to="/quiz">
				<img src="../img/quizz.png" className="image_footer" alt="Transport"/>
				</Link>
			</div>	
		</div>
		</div>
		</div>

	  );
	}
  }
  
  export default Info;