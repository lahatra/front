import React, { Component } from 'react';

class nouveauVoyage extends Component {
  render() {
    return (
      <div>
        <div id="titre">
			<div className ="element">
			<h2>Travelkit</h2>
			</div>
		</div>
		<div id="conteneur-nouveau-voyage">
			<div className="element">
			<button id="nouveau-voyage" type="submit">Nouveau Voyage</button>
			</div>
		</div>
    </div>
    );
  }
}

export default nouveauVoyage;
