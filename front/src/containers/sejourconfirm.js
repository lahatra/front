import React , {PropTypes} from 'react';
import Logo from '../assets/logo.png'
import Bouton from '../assets/bouton.png'
import { Link } from 'react-router-dom';

import ReactDOM from 'react-dom';
//import ReactSwap from 'react-swap';


//import './sejourconfirm.css';

class sejourconfirm extends React.Component {

  constructor(props , context) {
    super(props , context);
    this.state = {
        
    };
  }

render() {
    return(
        <div class="sejourconfirm-wrap">
  	        <div class="sejourconfirm-html">
	            <div class="sejourconfirm-form">

                        <div class="group">
                            <div class="header">
                                <div class="group">
                                    
                                    <div class="logo">
                                        <img src={Logo} alt="logo" class="logo"/>
                                    </div>

                                </div>

                                    <div class="group">
                                        <label class="title">Details de Séjour</label>
                                    </div>

                            </div>
                        </div>

                        <div class="first_question">
                            Prévoyez-vous de voyager à plus de 3 000 mètres?
                        </div>

                       <div class="button_confirm">
                            <Link  to="/sejourpre">
                                <input type="submit" class="yes" value="OUI" />
                            </Link>
                            <Link  to="/sejourpre">
                                <input type="submit" class="no" value="NON" />
                            </Link>
                       </div>

                       <div class="ignore">
                            <Link  to="/sejourpre">
                                <h3>Passer</h3>     
                            </Link>
                       </div>

                </div>                
            </div>                    
        </div>
    );
  }
}
export default sejourconfirm;
