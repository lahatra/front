import React from 'react';
import Logo from '../assets/logo.png';
import { Link } from 'react-router-dom';

import meningoque from '../data/meningoque'
import palu  from '../data/palu'
import Header4 from '../components/header4';
import dengue from '../data/dengue';
import rage from '../data/rage';

import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

const maladiesArray = [
  {
    id: 1,
    name: 'dengue',
  },
  {
    id: 2,
    name: 'rage',
  },
  {
    id: 3,
    name: 'meningoque',
  },
  {
    id: 4,
    name: 'palu',
  },

];

const paysValue = {
  id: 23,
  pays: 'Benin',
};
const meningoquePaysCheck = [
  {
    id: 1,
    pays: 'Arabie saoudite',
  },
];
const dengueM = (pays) => {
  const risque = dengue.filter(item => item.id === pays.id);
  if (risque.length > 0 && risque[0].risque !== 'NON') {
    return `Je prévois de me rendre notamment dans la zone suivante :${pays.pays}"`;
  }
  return false;
};

const rageM = (pays) => {
  const risque = rage.filter(item => item.id === pays.id);
  if (risque.length > 0 && (risque[0].risque !== 'AUCUN' || risque[0].risque !== 'NON CONNU')) {
    if (risque[0].risque === 'FAIBLE') {
      return 'Je serai en contact DIRECT et REGULIER avec des animaux CARNIVORES SAUVAGES et/ou des CHAUVES-SOURIS (concerne donc surtout certaines professions et activités très spécifiques)';
    } else if (risque[0].risque === 'MODERE') {
      return 'Je prévois de voyager dans des zones isolées (je ne pourrais pas avoir un avis médical dans la journée en cas de besoin) où  je serai en contact direct avec des animaux SAUVAGES et/ou des CHAUVES-SOURIS';
    } else if (risque[0].risque === 'HAUT') {
      return 'Je serai en contact direct avec des animaux domestiques, et/ou carnivores sauvages et/ou des chauves-souris';
    }
  }
  return false;
};
const meningoqueM = (pays) => {
  const risque = meningoque.filter(item => item.id === pays.id);
  if (risque.length > 0 && (risque[0].risque !== 'OUI')) {
    return false;
  }
  const paysChe = risque[0].pays;
  if (paysChe == 'Arabie saoudite') {
    return 'Je me rends en Arabie Saoudite dans le cadre du pèlerinage';
  }
  // PETIT ALGO DE TRAITEMENT DE DATE COMPATIBLE
  return "Je voyage habituellement dans des conditions de contacts étroits et prolongés avec des personnes potentiellement malades ?  Voici quelques exemples :  - contacts étroits : vie en dortoir, contact avec la salive : baisers, partage de verres ou couverts; éternuements et toux rapprochées, ...  - contacts prolongés :  contacts courts mais répétés et/ou plus d'une heure type transport collectif longue distance, vie commune, etc...";
};

const getUnique = (arr, comp) => {
  const unique = arr
    .map(e => e[comp])
    .map((e, i, final) => final.indexOf(e) === i && i)
    .filter(e => arr[e]).map(e => arr[e]);

  return unique;
};

// (){}[] : ''
class sejour extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      findPays: [],
      paysCurrent: {},
      maladieCurrent: [],
      paysDetail: {
        v1: [],
        v2: [],
      },
      vaccin: {
        v1: [],
        v2: [],
      },
      i: 0,
      questions: [],
        i: 0,
    };
  }

  componentDidMount() {
    const { one_voyage, voyage } = this.props.voyage;
    console.log('one_voyage, voyage', one_voyage, voyage);
    const { pays } = this.props.pays;
    const { maladie } = this.props.maladie;
    const { sejour } = this.props.sejour;
    // const { vaccin } = this.props.vaccin;
    // if (!pays || !(voyage.length > 0)) return;
    // const p = voyage.filter(i => i._id !== one_voyage._id).map(k => pays.find(j => j._id === k.pays));
    // const findPays = p.filter(i => i);
    // console.log('findPays findPays', findPays);
    // const resD = {
    //   v1: [],
    //   v2: [],
    // };
    // const paysDetail = voyage.reduce((acc, i) => {
    //   const paysV = pays.find(j => j._id === i.pays);
    //   if (paysV) {
    //     console.log('paysV paysV', paysV);
    //     const date = new Date(i.dateArrive).getTime();
    //     const dateNew = new Date().getTime();
    //     if (date > dateNew){
    //       acc.v1.push({ paysV, voyage: i});
    //     } else {
    //       acc.v2.push({ paysV, i});
    //     }
    //   }
    //   return acc;
    // }, resD);
    // this.setState({ paysDetail });
    // console.log('paysDetail paysDetail', paysDetail);
    const paysVoyage = one_voyage && pays && pays.filter(i => one_voyage.pays === i._id);
    if(!paysVoyage) return;
    if (paysVoyage.length > 0 && maladie) {
      const ids = paysVoyage[0].maladie;
      const maladiePays = maladie.filter(i => ids.map(i => i._id).includes(i._id));
      if (maladiePays && maladiePays.length > 0) {
        const arrayIDS = maladiePays.reduce((acc, j )=> {
          const sejourArray = j.sejour;
          console.log('iiiiiiiiii======>', sejourArray);
          sejourArray && acc.push(...sejourArray);
          return acc;
        }, []);
        if (arrayIDS) {
          const idsSejours = getUnique(arrayIDS, '_id').map(i => i._id);
          const questions = sejour.filter(i => idsSejours.includes(i._id));
          this.setState({ questions });
        }
      }
    }

    console.log('paysVoyage', paysVoyage);
    // paysID && this.setState({ paysCurrent: paysID[0] });
    // if(!(paysID.length > 0)) return;
    // const maladieIDS = paysID[0].maladie && paysID[0].maladie.map(i => i._id);
    // const maladieCurrent = maladie.filter(i => maladieIDS.includes(i._id));
    // maladieCurrent && this.setState({ maladieCurrent });
    // findPays && this.setState({ findPays });
    // console.log('findPays findPays', findPays[0]);
    // if (paysID && paysID.length > 0) {
    //   const maladieIDS = paysID[0].maladie && paysID[0].maladie.map(i => i._id);
    //   const res = {
    //     v1: [],
    //     v2: [],
    //   };

    //   const vaccinDetail = maladie && maladie.reduce((acc, curr) => {
    //     if (maladieIDS && maladieIDS.includes(curr._id)) {
    //       const mIDS = curr.vaccin.map(i => i._id);
    //       const s = vaccin && vaccin.filter(i => mIDS.includes(i._id));
    //       s && acc.v1.push(...s);
    //       const mIDSugg = curr.vaccinSugg.map(i => i._id);
    //       const s2 = vaccin && vaccin.filter(i => mIDSugg.includes(i._id));
    //       s2 && acc.v2.push(...s2);
    //     }
    //     return acc;
    //   }, res);
    //   const vs = vaccinDetail ? { v1: getUnique(vaccinDetail.v1, '_id'), v2: getUnique(vaccinDetail.v2, '_id') } : [];
    //   console.log('vaccin', vs);
    //   this.setState({ vaccin: vs });
    // }
    // this.props.getSejour(maladie[0]._id);
  }
    voyagealtitude = () => {
      this.props.history.push('/accueil');
    }

  getMaladie = (pays) => {
    const questions = maladiesArray.reduce((groups, currentValue) => {
      if (currentValue.id === 1 && dengueM(pays)) groups.push(dengueM(pays));
      if (currentValue.id === 2 && rageM(pays)) groups.push(rageM(pays));
      if (currentValue.id === 3 && meningoqueM(pays)) groups.push(meningoqueM(pays));
      return groups;
    }, []);
    if (questions.length === 0) {
      this.props.history.push('/accueil');
    }

    this.setState({ questions });
  }

  go = () => {
    const { questions, i } = this.state;
    if (questions.length > i + 1) {
      this.setState({ i: i + 1 });
    } else {
      this.props.history.push('/accueil');
    }
  }

  render() {
    const { questions, i } = this.state;
    const { voyage } = this.props.voyage;
    console.log('this.state.', questions, i);
    if (!(questions.length > 0)) {
      return (
        <div>
        <Header4 />
      <div className="sejour-wrap">
        <div className="sejour-html">
          <div className="sejour-form">
            <div className="group">
              <div className="header">
                <div className="group">

                  <div className="logo">
                    <img src={Logo} alt="logo" className="logo" />
                  </div>
                </div>

              </div>
            </div>
            <div id="conteneur2">
              <div className="ignore">
                <button>
                  <h3 onClick={this.go}>PASSER</h3>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
        </div>
      );
    }
    return (
      <div>
        <Header4 />
      <div className="sejour-wrap">
        <div className="sejour-html">
          <div className="sejour-form">
            <div className="group">
              <div className="header">
                <div className="group">

                  <div className="logo">
                    <img src={Logo} alt="logo" className="logo" />
                  </div>
                </div>

              </div>
            </div>
            <div id="conteneur">
              <label className="question">
                {questions[i].description}
              </label>
            </div>

            <div id="conteneur2">
              {questions[i].isQuestion &&
              <div className="button_confirm">
                <button onClick={this.go} className="yes"> OUI </button>
                <button onClick={this.go} className="no"> NON </button>
              </div>}
              <div className="ignore">
                <button>
                  <h3 onClick={this.go}>PASSER</h3>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
        </div>
    );
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(sejour);
