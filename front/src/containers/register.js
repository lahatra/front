import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header2 from '../components/header2';
import TextFieldGroup from '../common/TextFieldGroup';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      password2: '',
      errors: {},
    };
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      // this.props.history.push('/premiervoyage');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit = (e) => {
    e.preventDefault();

    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2,
    };

    this.props.registerUser(newUser, this.cb);
  }

  cb = () => {
    console.log('dvvssvvsvs');
    this.props.getVoyage();
    this.props.getChecklistUser();
    this.props.getSecoursUser();
    this.props.history.push('/premiervoyage');
  }

  register = async () => {
    if (this.state.password === this.state.password2) {
      const newUser = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        password2: this.state.password2,
      };
      await this.props.registerUser(newUser);
      console.log('this.props.registerUser');
      this.props.getVoyage();
      this.props.getChecklistUser();
      this.props.getSecoursUser();
      this.props.history.push('/premiervoyage');
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div>
        <Header2/>
      <div className="login-page">
       
          <div id="titre">
            <div className="element">
              <h1>Créer votre compte </h1>
             </div>
           </div>   
             <div id="conteneur-register">
              <div className="form">
              <form noValidate onSubmit={this.onSubmit} className="login-form">
                <TextFieldGroup
                  placeholder="Name"
                  name="name"
                  value={this.state.name}
                  onChange={this.onChange}
                  error={errors.name}
                />
                <TextFieldGroup
                  placeholder="Email"
                  name="email"
                  type="email"
                  value={this.state.email}
                  onChange={this.onChange}
                  error={errors.email}
                
                />
                <TextFieldGroup
                  placeholder="Password"
                  name="password"
                  type="password"
                  value={this.state.password}
                  onChange={this.onChange}
                  error={errors.password}
                />
                <TextFieldGroup
                  placeholder="Confirm Password"
                  name="password2"
                  type="password"
                  value={this.state.password2}
                  onChange={this.onChange}
                  error={errors.password2}
                />
                <button type="submit" >Enregister</button>
              </form>
            </div>
          </div>
          </div>
          </div>
       
   
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);
