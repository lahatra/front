import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class Boiremanger extends Component {
  login = () => {
    console.log("login");
  }
// style="padding-top: 120px;"
  render() {
    return (

<div>
	<Header4/>
<div className="sante-page">				
			   <div id="titre">
					<div className="element">
						<label className="title">Animaux</label>
					</div>
				</div>
	     <div className="group">
				<label className="description">  
				Le risque principal lié aux animaux est la rage. Elle existe dans tous les pays du monde et ma destination est considéré à risque.

Il existe un vaccin mais il n’est pas prescrit systématiquement à tous les voyageurs. Il est proposé dans mon carnet de vaccination selon mon Profil voyageur.

La rage est une maladie grave et mortelle qu’il est facilement évitable.

Les morsures ne sont pas le seul type de contact à risque. Il existe différents niveaux de risques qui vont du mordillage de la peau nue, griffures ou égratignures superficielles sans saignement aux morsures ou griffures traversant la peau, léchages d’une peau lésée (même une égratignure), contamination de la bouche ou du nez par de la salive après léchage, etc…

Il est donc fortement déconseillé d’approcher un animal, domestique ou non, dont le propriétaire n’est pas clairement identifié. Dans certains hébergements, les chats et chiens errants sont tolérés car ils chassent les petits animaux nuisibles, mais personne ne s’en occupe réellement pour autant. Un animal contaminé peut ne présenter aucun symptôme visible et être malgré tout contagieux.

			</label>
			</div>
<div id="conteneur">
<div className="element">
<img src="../img/chien.png" className="image" alt="chien" />
</div>
</div>
<div className="group">
				<label className="description">  
En cas de contact, il est indispensable de demander un avis médical, de préférence auprès d’un centre antirabique, à défaut d’un autre médecin qui pourra lui-même consulter le centre antirabique si besoin.

Est-ce que tous les contacts sont à risque ?
Non, seul le léchage sur une peau saine et le fait de toucher ou caresser un animal n’est pas à risque de rage. Mais il est souvent difficile d’en avoir la certitude ou de prévoir le comportement d’un animal, même quand il est amical. Au moindre doute, je demande un avis médical !

Je suis vacciné, donc protégé ?
Non, la vaccination diminue le risque mais ne protège pas à 100%. Le traitement d’une rage doit intervenir rapidement après un contact. Il consiste en plusieurs vaccinations complémentaires successives sur plusieurs jours. Pour les personnes non vaccinées, il existe un traitement supplémentaire, mais celui-ci coûte cher et surtout il n’est pas disponible dans tous les pays du monde ou uniquement dans les plus grands hôpitaux. L’intérêt de la vaccination lorsqu’elle est recommandée est donc aussi de se passer de ce deuxième traitement.
</label>
			</div>

		
		<div className="group">
	<div className="footer">
				<Link to="/info">
					<img src="../img/info.png" className="image_footer" alt="Infos" />
				</Link>
				<Link to="/moustique">
					<img src="../img/moustique.png" className="image_footer" alt="Moustique"/>				</Link>
				<Link to="/soleil">
					<img src="../img/soleil.png" className="image_footer" alt="soleil2"/>
				</Link>
				<Link to="/baignade">
					<img src="../img/baignade.png" className="image_footer" alt="baignade"/>
				</Link>
				<Link to="/animaux">
					<img src="../img/animaux.png" className="image_footer" alt="renard"/>
				</Link>
				<Link to="/quiz">
				<img src="../img/quizz.png" className="image_footer" alt="Transport"/>
				</Link>
	</div>	</div>
	</div>
	</div>
		

    );
  }
}

export default Boiremanger;
