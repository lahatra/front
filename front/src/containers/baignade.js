import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class Baignade extends Component {

	render() {
	  return (
		  <div>
			  <Header4/>
		  	<div className="sante-page">				
			   <div id="titre">
				<div className="element">
					<label className="title">Baignade </label>
				</div>
			</div>
	     <div className="group">
			<label className="description">En résumé : Je me renseigne localement, je respecte strictement les consignes de sécurité et les panneaux d’information. </label>
			<div className="tab blue">
				<input id="tab-four" type="radio" name="tabs2" />
					<label htmlFor="tab-four">En savoir plus</label>
				<div className="tab-content">
					<p>Dans tous les lieux de baignades, ils existent des risques : infections, noyade, envenimations.
						Les envenimations sont causées par la faune et la flore aquatique (poissons, coraux, méduses, même échoués). Certains animaux s’attaquent aux êtres humains, mais uniquement s’ils se sentent menacés. Je respecte donc simplement leur territoire et n’essaie pas de « jouer » avec eux.
					</p>
					<p>Ces risques sont très variables, d’un pays à l’autre, d’une région à l’autre, et parfois même au sein d’une même zone de baignade. Il convient donc de se renseigner localement, de respecter strictement les consignes de sécurité. Les lieux de baignades communs ou touristiques disposent souvent de panneau d’information.
						En effet, je ne peux pas me fier uniquement à ce que je vois pour m’assurer de la sureté de la baignade.</p>
					<p><img src="../img/plage_meduse.png" className="image_toggle" alt="baignade"/></p>
 
				</div>
			</div>
		<div className="tab blue">
				<input id="tab-five" type="radio" name="tabs2"/>
					<label htmlFor="tab-five">Les risques d'infections</label>
			<div className="tab-content">
				<p>Les risques d’infections lors de baignades sont surtout présents dans les eaux stagnantes (type lac, étang, etc…), j’évite de m’y baigner et autant que possible de marcher pieds nus et de m’assoir à même le sol sans serviette.</p>
			</div>
		</div>
		<div className="tab blue">
				<input id="tab-six" type="radio" name="tabs2"/>
					<label htmlFor="tab-six">Comment éviter les accidents </label>
				<div className="tab-content">
					<p>Le premier réflexe pour prévenir les noyades est de me renseigner localement sur les zones de baignades et de respecter les interdits et les consignes.
Pour éviter une hydrocution qui peut me faire perdre connaissance brutalement, il vaut mieux mouiller une partie de mon corps avant la baignade (cou, visage) et rentrer 
</p>
				</div>
		</div>
 
	<div className="footer">
				<Link to="/info">
					<img src="../img/info.png" className="image_footer" alt="Infos" />
				</Link>
				<Link to="/moustique">
					<img src="../img/moustique.png" className="image_footer" alt="Moustique"/>				</Link>
				<Link to="/soleil">
					<img src="../img/soleil.png" className="image_footer" alt="soleil2"/>
				</Link>
				<Link to="/baignade">
					<img src="../img/baignade.png" className="image_footer" alt="baignade"/>
				</Link>
				<Link to="/animaux">
					<img src="../img/animaux.png" className="image_footer" alt="renard"/>
				</Link>
				<Link to="/quiz">
				<img src="../img/quizz.png" className="image_footer" alt="Transport"/>
				</Link>
	</div>	
	
	</div>
			</div>
			</div>
	  );
	}
  }
  
  export default Baignade;