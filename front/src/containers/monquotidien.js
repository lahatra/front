import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class Moustiquetique extends Component {

  render() {
    return (
    <div>
		<Header4/>
        	<div className="quotidien-page">				
		<div id="titre">
			<div className ="element">
			<label className="title">Mon quotidien</label>
			</div>
		</div>
			<label className="title3">Pérou : 14/06/2019 - 16/07/2019</label>
			
		
	
			<div id="conteneur">
				<div ClassName="element">
						<Link to="/info">
							<img src="../img/info2.png" className="image-quotidien" alt="info" />
						</Link>
						<br></br>
				<label className="description1">Infos générales</label>
					</div>
					<div ClassName="element">
						<Link to="/alimentation">
							<img src="../img/aliment2.png" className="image-quotidien" alt="aliment" />
						</Link>
						<br></br>
				<label className="description1">Boire et manger</label>
					</div>
					
			</div>
			<div id="conteneur">
			<div ClassName="element">
						<Link to="/moustique">
							<img src="../img/moustique2.png" className="image-quotidien" alt="moustique" />
						</Link>
						<br></br>
				<label className="description1">Moustiques et tiques</label>
					</div>
				<div ClassName="element">
				<Link to="/soleil">
					<img src="../img/soleil2.png" className="image-quotidien" alt="soleil" />
				</Link><br></br>
				<label className="description1">Soleil</label>
				</div>
				
				</div>
				<div id="conteneur">
				<div ClassName="element">
				<Link to="/baignade">
					<img src="../img/baignade2.png" className="image-quotidien" alt="baignade" />
				</Link>
				<br></br>
				<label className="description1">Baignade </label>
				</div>
				<div ClassName="element">
				<Link to="/animaux">
					<img src="../img/animaux2.png" className="image-quotidien" alt="animaux" />
				</Link>
				<br></br>
				<label className="description1">Animaux </label>
				</div>
				</div>
				<div id="conteneur">
				<div ClassName="element">
				<Link to="/quizz">
					<img src="../img/quizz2.png" className="image-quotidien" alt="quizz" />
				</Link>
				<br></br>
				<label className="description1">Quizz</label>
				</div>
				</div>
			</div>	
		</div>


    );
  }
}

export default Moustiquetique;
