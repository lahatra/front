import React, { Component } from 'react';
import Header2 from '../components/header2';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

class PremierVoyage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checklistList: [],
    };
  }

  async componentDidMount() {
    await this.props.getVoyage();
    // const { voyage } = this.props.voyage;
    // console.log('this.props.voyage ====xxxxxxxxxxxx>', this.props.voyage);
    // if (voyage && voyage.length > 0) {
    //   this.props.history && this.props.history.push('/accueil');
    // }
  }

	madestination = () => {
	  this.props.history.push('/madestination');
	}

	render() {
	  const { voyage } = this.props.voyage;
	  console.log('this.props.voyage ====xxxxxxxxxxxx>', this.props.voyage);
	  if (voyage && voyage.length > 0) {
	    this.props.history && this.props.history.push('/accueil');
	  }

	  console.log('Render this.props.voyage ====xxxxxxxxxxxx>', this.props.voyage);
	  return (
	    <div>
    <Header2 />
	      <div id="titre">
	        <div className="element">
    <h1>Travelkit</h1>
  </div>
  </div>
    <div id="conteneur">
  <div className="element">
  <button id="voyage" onClick={this.madestination}>Mon Premier Voyage</button>
	        </div>
	      </div>
  </div>
	  );
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PremierVoyage);
