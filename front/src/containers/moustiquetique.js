import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class Moustiquetique extends Component {

  render() {
    return (
    <div>
			<Header4/>
        	<div className="sante-page">				
		<div id="titre">
			<div className ="element">
			<label className="title">Moustiques et Tiques </label>
			</div>
		</div>
		<div className='group'>
		<label className="description">Pour ce voyage, il y a un risque de :</label>
		<Link to="">
		<div className="sante">
		<img src="../img/moustique.png" className="image" alt="Vaccins" />
		<span className="text">Moustiques</span>
		</div>
		</Link>
		</div>
		<div className='group'>
		<Link to="">
		<div className="sante">
		<img src="../img/tique.png" className="image" alt="Vaccins" />
		<span className="text">Tiques </span>
		</div>
		</Link>
		</div>
		<div className='group' >

			<div className="footer">
				<Link to="/info">
					<img src="../img/info.png" className="image_footer" alt="Infos" />
				</Link>
				<Link to="/moustique">
					<img src="../img/moustique.png" className="image_footer" alt="Moustique" />
				</Link>
				<Link to="/soleil">
					<img src="../img/soleil.png" className="image_footer" alt="soleil2" />
				</Link>
				<Link to="/baignade">
					<img src="../img/baignade.png" className="image_footer" alt="baignade" />
				</Link>
				<Link to="/animaux">
					<img src="../img/animaux.png" className="image_footer" alt="renard" />
				</Link>
				<Link to="/quiz">
				<img src="../img/quizz.png" className="image_footer" alt="Transport" />
				</Link>
			</div>	
		</div>
	</div>
	</div>
    );
  }
}

export default Moustiquetique;
