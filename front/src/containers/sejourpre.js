import React , { Component } from 'react';
import Logo from '../assets/logo.png'
import Bouton from '../assets/bouton.png'
import { Link } from 'react-router-dom';

import ReactDOM from 'react-dom';
import ReactSwap from 'react-swap';


import './sejourpre.css';

class sejourpre extends React.Component {

  constructor(props , context) {
    super(props , context);
    this.state = {
        show: false
    };
  }

  showModal = () => {
    this.setState({ show: true });
  };

  hideModal = () => {
    this.setState({ show: false });
  };

render() {
    return(
        <div class="sejourpre-wrap">
  	        <div class="sejourpre-html">
	            <div class="sejourpre-form">

                        <div class="group">
                            <div class="header">
                                <div class="group">
                                    
                                    <div class="logo">
                                        <img src={Logo} alt="logo" class="logo"/>
                                    </div>

                                </div>

                                    <div class="group">
                                        <label class="title">Details de Séjour</label>
                                    </div>

                            </div>
                        </div>

                        <div class="first_question">
                        Prévoyez-vous d'aller dans une de ces zones : Ayacucho, Junín, Loreto, Madre de Dios, Piura, San Martín et Tumbes ?
                        </div>

                       <div class="button_confirm">
                            <input type="submit" class="yes" value="OUI" show={this.state.show} handleClose={this.hideModal} onClick={this.showModal} />
                            <input type="submit" class="no" value="NON" onClick={this.showModal}/>
                       </div>

                       <div class="ignore">
                            <h3>Passer</h3>     
                       </div>

                </div>                
            </div>                    
        </div>
    );
  }
}

const container = document.createElement("div");
document.body.appendChild(container);
ReactDOM.render(<sejourpre />, container);

export default sejourpre
