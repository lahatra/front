import React, { Component } from 'react';
import { connect } from 'react-redux';
// import axios from 'axios';

// import config from '../redux/utils/config';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

const getUnique = (arr, comp) => {
  const unique = arr
    .map(e => e[comp])
    .map((e, i, final) => final.indexOf(e) === i && i)
    .filter(e => arr[e]).map(e => arr[e]);

  return unique;
};

class Header3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      findPays: [],
      paysCurrent: {},
      maladieCurrent: [],
      paysDetail: {
        v1: [],
        v2: [],
      },
      vaccin: {
        v1: [],
        v2: [],
      },
      i: 0,
    };
  }

  async componentDidMount() {
    await this.props.getPays();
    await this.props.getVoyage();
  }

  /*
  componentWillMout() {
    console.log('getV =====> componentWillMout');
    const { one_voyage, voyage } = this.props.voyage;
    const { user } = this.props.auth;
    const { pays } = this.props.pays;
    if (!pays || !(voyage.length > 0)) return;
    console.log('return =====> return');
    const p = voyage.filter(i => i._id !== one_voyage._id).map(k => pays.find(j => j._id === k.pays));
    const findPays = p.filter(i => i);
    const resD = {
      v1: [],
      v2: [],
    };
    const paysDetail = voyage.reduce((acc, i) => {
      const paysV = pays.filter(j => j._id === i.pays);
      if (paysV.length > 0) {
        const date = new Date(i.dateArrive).getTime();
        const dateNew = new Date().getTime();
        if (date > dateNew){
          acc.v1.push({ paysV: paysV[0], voyage: i});
        } else {
          acc.v2.push({ paysV: paysV[0], i});
        }
      } else {
        console.log('paysV paysVpaysV else', paysV);
      }
      return acc;
    }, resD);
    this.setState({ paysDetail });
  } */

  openNav = () => {
    document.getElementById('mySidenav').style.width = '250px';
    document.getElementById('main').style.marginRight = '250px';
  }

  closeNav = () => {
    document.getElementById('mySidenav').style.width = '0';
    document.getElementById('main').style.marginRight = '0';
  }
    
  monprofil = () => {
    this.props.history && this.props.history.push('/monprofil');
  }


  render() {
    let paysDetail = [];
    const { paysCurrent, findPays } = this.state;
    console.log('paysCurrent paysCurrent', paysCurrent);
    const { one_voyage, voyage } = this.props.voyage;
    const { user } = this.props.auth;
    // const getV = await axios.get(`${config.baseUrl}/api/voyage/user/${user.id}`);
    // console.log('getV =====>', getV);
    // console.log('one_voyage, voyage', one_voyage, voyage);
    const { pays } = this.props.pays;
    if (!(!pays || !(voyage.length > 0))){
      console.log('return =====> return');
      const p = voyage.filter(i => i._id !== one_voyage._id).map(k => pays.find(j => j._id === k.pays));
      const findPays = p.filter(i => i);
      const resD = {
        v1: [],
        v2: [],
      };
      paysDetail = voyage.reduce((acc, i) => {
        const paysV = pays.filter(j => j._id === i.pays);
        if (paysV.length > 0) {
          const date = new Date(i.dateArrive).getTime();
          const dateNew = new Date().getTime();
          if (date > dateNew){
            acc.v1.push({ paysV: paysV[0], voyage: i});
          } else {
            acc.v2.push({ paysV: paysV[0], i});
          }
        } else {
          console.log('paysV paysVpaysV else', paysV);
        }
        return acc;
      }, resD);
    }
    // this.setState({ paysDetail });
    return (
      <div>
        <div id="mySidenav" className="sidenav">
          <a href="javascript:void(0)" className="closebtn" onClick={this.closeNav}>&times;</a>
          <img src="../img/profile.png" alt="Infos" />
          <button onClick={this.monprofil}><p>Anna Martine</p> </button>
          <br />
          <a href="#"><p>Mon profil voyageur </p></a>
          <br />
          <a href="#"><p>Mes voyages prévus</p> </a>
          <br />
          {
            paysDetail.v1 && paysDetail.v1.map((i, j) => <div key={`v1 ${i.paysV._id} ${j}`}><a href="#"><p key={`v11 ${i.paysV._id} ${j}`}>- {i.paysV.name}</p></a><br /></div>)
          }
          <br />
          <a href="#"><p>Mes anciens voyages</p></a>
          <br />
          {
            paysDetail.v2 && paysDetail.v2.map(i => <div key={i.paysV._id}><a href="#"><p key={i.paysV._id}>- {i.paysV.name}</p></a><br /></div>)
          }
          <a href="#"><p>Mon abonnement </p></a>
          <br />
          <a href="#"><p>Mentions légales</p>  </a>
        </div>

        <div id="main">

          <div className="logo"><img src="../img/logo.png" alt="Infos" /></div>
          <span className="effet" onClick={this.openNav}>&#9776; </span>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header3);
