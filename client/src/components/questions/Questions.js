import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-responsive-modal';

import QuestionForm from './QuestionForm';
import QuestionFeed from './QuestionFeed';
import Spinner from '../common/Spinner';
import { getQuestions } from '../../actions/QuestionActions';

class Questions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data: {}
    };
  }

  onOpenModal = () => {
    this.setState({ open: true, data: {} });
  };

  onUpdate = data => {
    this.setState({ open: true, data });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    this.props.getQuestions();
  }

  render() {
    const { checklists, loading } = this.props.checklist;
    const { open, data } = this.state;
    let questionContent;

    if (checklists === null || loading) {
      questionContent = <Spinner />;
    } else {
      questionContent = <QuestionFeed checklists={checklists} onUpdate={this.onUpdate} />;
    }

    return (
      <div className="feed">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <button onClick={this.onOpenModal} type="submit" className="btn btn-dark">
                  Ajout
                </button>
              {questionContent}
            </div>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal} center>
          <QuestionForm onClose={this.onCloseModal} data={data} />
        </Modal>
      </div>
    );
  }
}

Questions.propTypes = {
  getQuestions: PropTypes.func.isRequired,
  checklist: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  checklist: state.checklist,
});

export default connect(mapStateToProps, { getQuestions })(Questions);
