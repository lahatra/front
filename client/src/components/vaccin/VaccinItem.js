import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

import { deleteVaccin, updateVaccin } from '../../actions/VaccinActions';
import attribut from '../../attributs';

class VaccinItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      rappel: '',
      errors: {},
      open: false,
      id: null,
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
 
  onOpenModal = (data) => {
    this.setState({ open: true, id: data._id, text: data.name, rappel: data.rappel, });
  };
 
  onCloseModal = () => {
    this.setState({ open: false, id: null });
  };

  onDeleteClick(id) {
    this.props.deleteVaccin(id);
  }

  onUpdateClick(id) {
    const description = {
      name: this.state.text,
      rappel: this.state.rappel,
      id,
    };
    this.onCloseModal();
    this.props.updateVaccin(description);
  }

  render() {
    const { vaccin } = this.props;
    const { open, errors } = this.state;

    return (
      <div className="card card-body mb-3">
        <div className="row">
          <div className="col-md-10">
            {attribut.vaccin.map((key, i) => <div key={i}>{key}: {vaccin[key]}</div>)}
              <span>
                <button
                  onClick={this.onDeleteClick.bind(this, vaccin._id)}
                  type="button"
                  className="btn btn-danger mr-1"
                >
                  <i className="fas fa-times" />
                </button>
                <button
                  onClick={() => this.props.onUpdate(vaccin)}
                  type="button"
                  className="btn btn-info mr-1"
                >
                  update
                </button>
              </span>
          </div>
        </div>
      </div>
    );
  }
}

VaccinItem.defaultProps = {
  showActions: true
};

VaccinItem.propTypes = {
  deleteVaccin: PropTypes.func.isRequired,
  updateVaccin: PropTypes.func.isRequired,
  vaccin: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { deleteVaccin, updateVaccin })(
  VaccinItem
);
