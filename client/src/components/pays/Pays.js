import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-responsive-modal';
import PaysForm from './PaysForm';
import PaysFeed from './PaysFeed';
import Spinner from '../common/Spinner';
import { getPays } from '../../actions/PaysActions';
import { getMaladie } from '../../actions/MaladieActions';
import { getCentres } from '../../actions/CentreActions';
import { getMedecin } from '../../actions/MedecinActions';
import { getUrgence } from '../../actions/UrgenceActions';

class Pays extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data: {}
    };
  }

  onOpenModal = () => {
    console.log('dvsdvsv')
    this.setState({ open: true, data: {} });
  };

  onUpdate = data => {
    this.setState({ open: true, data });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  async componentWillMount() {
    await this.props.getPays();
    await this.props.getMaladie();
    await this.props.getMedecin();
    await this.props.getCentres();
    await this.props.getCentres();
    await this.props.getUrgence();
  }

  render() {
    const { pays, loading } = this.props.pays;
    const { open, data } = this.state;
    const { maladie } = this.props.maladie;
    const { medecin } = this.props.medecin;
    const { urgence } = this.props.urgence;

    console.log('urgence urgence', urgence);
    let paysContent;

    if (pays === null || loading) {
      paysContent = <Spinner />;
    } else {
      paysContent = <PaysFeed pays={pays} onUpdate={this.onUpdate} />;
    }

    return (
      <div className="feed">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
            <button onClick={this.onOpenModal} type="submit" className="btn btn-dark">
              Ajout
            </button>
              {/* <PaysForm
                maladie={maladie}
                medecin={medecin}
                centre={this.props.centre.centre}
              /> */}
              {paysContent}
            </div>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal} center>
          <PaysForm
            onClose={this.onCloseModal}
            data={data}
            maladie={maladie}
            medecin={medecin}
            urgence={urgence}
            centre={this.props.centre.centre}
          />
        </Modal>
      </div>
    );
  }
}

Pays.propTypes = {
  getUrgence: PropTypes.func.isRequired,
  getMaladie: PropTypes.func.isRequired,
  getMedecin: PropTypes.func.isRequired,
  getCentres: PropTypes.func.isRequired,
  getPays: PropTypes.func.isRequired,
  pays: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  pays: state.pays,
  maladie: state.maladie,
  centre: state.centre,
  medecin: state.medecin,
  urgence: state.urgence,
});

export default connect(mapStateToProps, { getPays, getMaladie, getMedecin, getCentres, getUrgence })(Pays);
